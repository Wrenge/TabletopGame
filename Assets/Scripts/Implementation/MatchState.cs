﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Implementation
{
    public class MatchState : MonoBehaviour
    {
        #region Properties

        public List<BaseCharacter> Characters { get; private set; }

        public int NumRounds { get; protected set; } = 0;
        public int TurnNumber { get; set; } = 0;

        public static MatchState Main => FindObjectOfType<MatchState>();

        #endregion

        #region Variables

        private HomePanel[] _homes;

        #endregion

        #region Events

        public event EventHandler<EventArgs> RoundStart;
        public event EventHandler<EventArgs> NewTurn;

        #endregion

        #region Methods

        private void Start()
        {
            _homes = GetComponents<HomePanel>();
            if (_homes == null || _homes.Length == 0)
            {
                throw new Exception("No home panels!");
            }

            OnRoundStart(EventArgs.Empty);
        }

        protected virtual void OnRoundStart(EventArgs e)
        {
            NumRounds++;
            TurnNumber = 0;
            var handler = RoundStart;
            handler?.Invoke(this, e);
        }

        public virtual void OnEndTurn(EventArgs e)
        {
            var handler = NewTurn;
            handler?.Invoke(this, e);

            Characters[TurnNumber].OnTurnEnd(EventArgs.Empty);
            TurnNumber++;
            if (TurnNumber > Characters.Count)
            {
                OnRoundStart(EventArgs.Empty);
            }

            Characters[TurnNumber].OnTurnStart(EventArgs.Empty);
        }

        #endregion
    }
}