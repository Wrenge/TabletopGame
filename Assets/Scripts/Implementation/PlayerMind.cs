﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Implementation
{
    public class PlayerMind : MonoBehaviour
    {
        #region Variables

        private MindState _mindState;

        private BasePanel _targetPanel;
        private int[] _path;
        private List<BasePanel> _availableNodes;

        #endregion

        #region Properties

        public BaseCharacter ControlledChar { get; set; }

        #endregion

        #region Events

        public event EventHandler<PossessEventArgs> PossessEvent;
        public event EventHandler<EventArgs> UnPossessEvent;

        #endregion

        #region Methods

        private void Update()
        {
            if (ControlledChar == null) return;

            switch (_mindState)
            {
                case MindState.None:
                    if(Input.GetKeyDown(KeyCode.Space))
                        EnterMindState(MindState.Move);
                    break;
                case MindState.Move:
                    if(Input.GetKeyDown(KeyCode.Escape))
                        EnterMindState(MindState.None);
                    ProcessMoveMindState();
                    break;
                case MindState.Fight:
                    break;
                case MindState.Dead:
                    break;
                case MindState.Spectate:
                    break;
                case MindState.Cards:
                    if(Input.GetKeyDown(KeyCode.Escape))
                        EnterMindState(MindState.None);
                    break;
                case MindState.Info:
                    if(Input.GetKeyDown(KeyCode.Escape))
                        EnterMindState(MindState.None);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(_mindState), _mindState, null);
            }
        }

        private void OnPossess(PossessEventArgs e)
        {
            if (ControlledChar != null) OnUnpossess(EventArgs.Empty);
            var handler = PossessEvent;
            handler?.Invoke(this, e);

            ControlledChar = e.PossesedChar;
            ControlledChar.OnPossess(this);
        }

        private void OnUnpossess(EventArgs e)
        {
            if (ControlledChar == null) throw new Exception("No ControlledChar");
            var handler = UnPossessEvent;
            handler?.Invoke(this, e);

            ControlledChar.OnUnPossess();
            ControlledChar = null;
        }

        private void EnterMindState(MindState mindState)
        {
            switch (mindState)
            {
                case MindState.None:
                    break;
                case MindState.Move:
                    GetAvailablePanels();
                    HighlightAvailableNodes();
                    break;
                case MindState.Fight:
                    break;
                case MindState.Dead:
                    break;
                case MindState.Spectate:
                    break;
                case MindState.Cards:
                    break;
                case MindState.Info:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mindState), mindState, null);
            }

            _mindState = mindState;
        }

        private void ProcessMoveMindState()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out var hit, 1000.0f)) return;
            var temp = hit.collider.gameObject.GetComponent<BasePanel>();
            if (temp != null && temp != _targetPanel && _availableNodes.Contains(temp))
            {
                UnHighlightPath();
                _targetPanel = hit.collider.gameObject.GetComponent<BasePanel>();
                _path = OrientedGraph.Main.BFS_FindPath(ControlledChar.CurrentPanelId, _targetPanel.Id);
                HighlightPath();

                if (!Input.GetKeyUp(KeyCode.Mouse0)) return;
                ControlledChar.GoToNode(_targetPanel.Id);
                _mindState = MindState.None;
            }
            else if (temp == null)
            {
                HighlightAvailableNodes();
            }
        }

        private void GetAvailablePanels()
        {
            var start = ControlledChar.CurrentPanelId;
            var range = ControlledChar.Mana;
            var panelIds = OrientedGraph.Main.BFS_FindInRange(start, range);
            var verts = OrientedGraph.Main.Vertices;

            _availableNodes.Clear();
            foreach (var id in panelIds)
            {
                _availableNodes.Add(verts[id]);
            }
        }

        private void HighlightPath()
        {
            var verts = OrientedGraph.Main.Vertices;
            foreach (var i in _path)
            {
                verts[i].Highlight();
            }
        }

        private void UnHighlightPath()
        {
            UnHighlightAvailableNodes();
            var verts = OrientedGraph.Main.Vertices;
            foreach (var i in _path)
            {
                verts[i].UnHighlight();
            }
        }

        private void HighlightAvailableNodes()
        {
            foreach (var node in _availableNodes)
            {
                node.Highlight();
            }
        }

        private void UnHighlightAvailableNodes()
        {
            foreach (var node in _availableNodes)
            {
                node.UnHighlight();
            }
        }
        
        public virtual void OnStand()
        {
            // TODO: Implement better behaviour
            ControlledChar.EndTurn();
        }

        public virtual void OnTurnStart()
        {
            EnterMindState(MindState.None);
        }

        public virtual void OnTurnEnd()
        {
            EnterMindState(MindState.Spectate);
        }

        #endregion
    }

    public class PossessEventArgs : EventArgs
    {
        public BaseCharacter PossesedChar { get; set; }
    }

    public enum MindState
    {
        None,
        Move,
        Cards,
        Info,
        Fight,
        Dead,
        Spectate
    }
}