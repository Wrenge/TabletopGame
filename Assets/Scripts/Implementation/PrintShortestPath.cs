﻿using UnityEngine;

namespace Implementation
{
    public class PrintShortestPath : MonoBehaviour
    {
        private int _clickNum;
        private BasePanel[] _panels;
        private OrientedGraph _graph;
        private int[] _lastHilit;

        private void Start()
        {
            _panels = new BasePanel[2];
            _graph = OrientedGraph.Main;
        }

        private void Update()
        {
            if (Input.GetButtonUp("Fire1"))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000.0f))
                {
                    var panel = hit.collider.gameObject.GetComponent<BasePanel>();
                    if (panel != null)
                    {
                        _panels[_clickNum] = panel;
                        _clickNum++;
                        if (_clickNum > 1)
                        {
                            _lastHilit = _graph.BFS_FindPath(_panels[0].Id, _panels[1].Id);
                            foreach (var i in _lastHilit)
                            {
                                _graph.Vertices[i].Highlight();
                            }

                            _clickNum %= 2;
                        }
                        else if (_lastHilit != null)
                        {
                            UnHighlight();
                        }

                        panel.Highlight();
                    }
                }
            }

            if (Input.GetButton("Fire2"))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000.0f))
                {
                    var panel = hit.collider.gameObject.GetComponent<BasePanel>();
                    if (panel != null)
                    {
                        UnHighlight();
                        _clickNum = 0;
                        _lastHilit = _graph.BFS_FindInRange(panel.Id, 3);
                        foreach (var i in _lastHilit)
                        {
                            _graph.Vertices[i].Highlight();
                        }
                    }
                }
            }
        }

        private void UnHighlight()
        {
            if (_lastHilit == null) return;

            foreach (var i in _graph.Vertices)
            {
                i.UnHighlight();
            }
        }
    }
}